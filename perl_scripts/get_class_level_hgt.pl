#cas.Cluster_4967	487.SAMN03197529.GCA_001064635_01257	specI_v3_Cluster4794	Bacteria	Proteobacteria	BetaproteobacteriaNeisseriales	Neisseriaceae	Neisseria
#cas.Cluster_4967	486.SAMN03197528.GCA_001064605_00740	specI_v3_Cluster4794	Bacteria	Proteobacteria	BetaproteobacteriaNeisseriales	Neisseriaceae	Neisseria
%cl = %tax = %cl2 = %count = %fam = %all = ();

	open("fh","raw_data/recombinase_hgt_cluster_master_file.txt") or die $!;
	while(<fh>) {
	chomp $_;
	my @l = split (/\t/,$_);
	($f,$j) = split (/\./,$l[0]);

		if ($_ !~ m/^rec_cluster/) {
		#class
		if (!exists $cl2{$l[0]}{$l[7]}) {
                push @{$cl{$l[0]}}, $l[7];
                $tax{$l[0]}{$l[7]} = $l[5];
                $cl2{$l[0]}{$l[7]} = 1;
                $count{$l[0]}{$l[5]} = $count{$l[0]}{$l[5]} + 1 if !exists $fam{$l[0]}{$l[7]};
		$fam{$l[0]}{$l[7]} = $l[1];
		}

                $all{$f}{$l[0]} = 1;
		}

	}
	close fh;

	%done = ();
	foreach $f (keys %all) {

		foreach $c (sort keys %{$all{$f}}) {
		#print "$f\t$c\n";
			for($i=0;$i<=$#{$cl{$c}};$i++) {
			$family = $cl{$c}[$i];	
			$class = $tax{$c}{$family};
			print "$c\t$family\t$class\t$fam{$c}{$family}\n" if !exists $done{$c}{$family};
			$done{$c}{$family} = 1;
			}

		}
	}
