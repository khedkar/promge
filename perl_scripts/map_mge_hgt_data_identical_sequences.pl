#1261069.SAMN02436910.HMPREF1584_00144.Fr12_27932383	Int_Tn916	5475	specI_v3_Cluster3576	1261069.SAMN02436910	ACC	 1261069.SAMN02436910.KE346735	47813	53288	1261069.SAMN02436910.HMPREF1584_00142.Fr12_27932381 1261069.SAMN02436910.HMPREF1584_00143.Fr12_27932382 1261069.SAMN02436910.HMPREF1584_00144.Fr12_27932383 1261069.SAMN02436910.HMPREF1584_00145.Fr12_27932384 1261069.SAMN02436910.HMPREF1584_00146.Fr12_27932385 1261069.SAMN02436910.HMPREF1584_00147.Fr12_221413 1261069.SAMN02436910.HMPREF1584_00148.Fr12_221414 1261069.SAMN02436910.HMPREF1584_00149.Fr12_27932386 1261069.SAMN02436910.HMPREF1584_00150.Fr12_221415
open("fh","raw_data/recombinase_islands.txt") or die $!;
while(<fh>) {
chomp $_;

my @l = split (/\t/,$_);
my @acc = split (/\s/,$l[9]);


        $is = "$l[6]".":"."$l[7]"."-$l[8]";
	$is =~ s/\s//g;	

	for($i=0;$i<=$#acc;$i++) {
	my @pid = split (/\./,$acc[$i]);
	$prot = "$pid[0]".".$pid[1]".".$pid[2]";
	$seen{$prot} = $is;
	#print "$acc[$i]\t$prot\t?$is?\n";
	}

	$saw{$is} = 1;

}
close fh;

#island	mge	mge_pa
#100053.SAMN02947772.JQGW01000007:25819-29946	#IS_Tn	1
#100053.SAMN02947772.JQGW01000007:56267-67015	#IS_Tn	1
open("fh2","processed_data/mge_bins_final_collapsed.txt") or die $!;
while(<fh2>) {
chomp $_;
my @l2 = split (/\t/,$_);

#$l2[0] =~ s/\s//g;
$req = "$l2[1]".":$l2[2]";
$req =~ s/\#//g;

	if ($_ !~ m/^#/ and exists $saw{$l2[0]}) {
	push @{$mge{$l2[0]}}, $req;
	}

#print "$l2[0]\t$req\n";
}
close fh2;

open("fhb","raw_data/recombinase_besthit.txt") or die $!;
while(<fhb>) {
chomp $_;
my @l = split (/\s+/,$_);
$rec_hmm{$l[0]} = $l[2];
}
close fhb;


open(OUT,">processed_data/mapped_MGE_top_tax_file_final_100_100.txt");
	
#rec_cluster	cumul	top_tax	kingdom	phylum	class	order	family	prot
#cas.Cluster_1526	1	family	0	0	0	0	1	1235788.SAMN01730986.C802_00889
	open("fh3","processed_data/pre_MGE_top_tax_file_final_100_100.txt") or die $!;
	while(<fh3>) {
	chomp $_;

	my @l = split (/\t/,$_);

		if (exists $seen{$l[8]}) {
		$island = $seen{$l[8]};
		$m = ();

			for($i=0;$i<=$#{$mge{$island}};$i++) {
			$m .= "&$mge{$island}[$i]";
			}

		print OUT "$island\t$m\t$rec_hmm{$l[8]}\t$_\n";
		}

		if (!exists $seen{$l[8]}) {
		print OUT "ISLAND_ND\tMGE_ND\t$rec_hmm{$l[8]}\t$_\n";
                }

	}
	close fh3;
	close OUT;

