#IS_Tn	Phage	Phage_like	CE	Integron	MI	Hotspot	UC	Cellular	island	island_size	prot_count	phage_count	CONJ_T4SS	mgeR
#1	0	0	0	0	0	0	0	0	1000565.PRJNA64645.AFHG01000010:116-1165	1049	1	0	0	&DDE_Tnp_1
open("fh","raw_data/mge_bins_final.txt") or die $!;
while (<fh>) {
chomp $_;
my @l = split (/\t/,$_);
$l[9] =~ s/\s//g;
my @g = split (/\./,$l[9]);
$genome = "$g[0]".".$g[1]";
$seen{$genome}{$l[9]} = $_;
}
close fh;


#taxa information
open("fh1","raw_data/species_with_atleast_2genomes.list") or die $!;
while(<fh1>) {
chomp $_;

        ($specI,$g,@b) = split (/\t/,$_);
        $present{$g} = $specI;

}
close fh1;



#specI_cluster	ref_mOTU_cluster	kingdom	phylum	class	order	family	genus	refOTUs
#specI_v2_4392	ref_mOTU_v2_4392	2 Bacteria	1090 Chlorobi	191410 Chlorobia	191411 Chlorobiales	191412 Chlorobiaceae	100715 Chloroherpeton	100716 Chloroherpeton thalassium
#100053.SAMN02947772	2 Bacteria	203691 Spirochaetes	203692 Spirochaetia	1643688 Leptospirales	170 Leptospiraceae	171 Leptospira	100053 Leptospira alexanderi
open("fh1","raw_data/genomes_taxonomy.txt") or die $!;
while(<fh1>) {
chomp $_;

	my @tax = split (/\t/,$_);
	$tax[2] =~ s/(\d+) //g;
	$tax[3] =~ s/(\d+) //g;
	$tax[4] =~ s/(\d+) //g;
	$tax[1] =~ s/(\d+) //g; 

	$kn{$tax[0]} = $tax[1];
	$phy{$tax[0]} = $tax[2];
	$class{$tax[0]} = $tax[3];
	$genus{$tax[0]} = $tax[4];
	#print "##$tax[0]##\n";
}
close fh1;

	print "Genome\tspecI\tkingdom\tphylum\tclass\tgenus\tIS_Tn\tPhage\tPhage_like\tCE\tIntegron\tMI\tHotspot\tCellular\n";
	foreach $g (keys %seen) {
	$tn = $ph = $pli = $ice = $mi = $int = $cell = $nmi = 0; 

		foreach $is (keys %{$seen{$g}}) {
		my @l1 = split (/\t/,$seen{$g}{$is});


		$int += $l1[4];
		$cell += $l1[8];
		$tn += $l1[0];
		$nmi += $l1[6];
		$ice += $l1[3];
		
			#phage/mi
			if ($l1[5] eq 1 and $l1[1] eq 1 and $l1[12] >= 2) {
			$ph += $l1[1];
			}


			elsif ($l1[5] eq 1 and $l1[1] eq 1 and $l1[12] < 2) {
                        $mi += $l1[5];
                        }

			elsif ($l1[5] eq 0 and $l1[1] eq 1) {
                        $ph += $l1[1];
                        }
	
			
			#pli/mi
			if ($l1[5] eq 1 and $l1[2] eq 1 and $l1[14] =~ m/Relaxase|Rep_|Mob_|TrwC/) {
                        $mi += $l1[5];
                        }


                        elsif ($l1[5] eq 1 and $l1[2] eq 1 and $l1[14] !~ m/Relaxase|Rep_|Mob_|TrwC/) {
                        $pli += $l1[2];
                        }


			elsif ($l1[5] eq 0 and $l1[2] eq 1) {
                        $pli += $l1[2];
                        }

			if ($l1[5] eq 1 and $l1[2] eq 0 and $l1[1] eq 0) {
                        $mi += $l1[5];
                        }
		
		}

	print "$g\t$present{$g}\t$kn{$g}\t$phy{$g}\t$class{$g}\t$genus{$g}\t$tn\t$ph\t$pli\t$ice\t$int\t$mi\t$nmi\t$cell\n";		
	}
