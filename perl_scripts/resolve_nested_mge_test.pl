##rec	Is_Tn	Phage	ICE	IME	MI	Cellular
#huh_y1	1	0	0	0	0	0
#huh_y2	1	0	0	0	0	0
#rec_subfamily	IS_Tn	Phage	CE	Integron	Cellular
#huh_y1	1	0	0	0	0
#huh_y2	1	0	0	0	0
open("fh","raw_data/mge_rules_ms.txt") or die $!;
while(<fh>) {
chomp $_;
my @l = split (/\t/,$_);
$tn{$l[0]} = $l[1];
$ph{$l[0]} = $l[2];
$ce{$l[0]} = $l[3];
#$ime{$l[0]} = $l[4];
#$mi{$l[0]} = $l[5];
$int{$l[0]} = $l[4]; 
$cl{$l[0]} = $l[5];
}
close fh;
#1235788.SAMN01730986.KE159493:1006981-1038590	&IS_Tn:3	Casposons	cas.Cluster_1526	family	0	0	0	0	1	1235788.SAMN01730986.C802_00889

#1299322.SAMN02641602.JAOI01000005:654202-705645	&IS_Tn:1&CE:1&UC:1	Myc	genus	tyr.Cluster_99486	1299322.SAMN02641602.I541_5280	tyr.Cluster_99486	1299322.SAMN02641602.I541_5280	specI_v3_00353	Bacteria	Actinobacteria	Actinobacteria	Corynebacteriales	Mycobacteriaceae	670516 Mycobacteroides
#open("fh1","all-95_100_cdhit.fa.clstr_tax_p2f_hgt_mge.txt") or die $!;
open("fh1","$ARGV[0]") or die $!;
while(<fh1>) {
chomp $_;
my @l = split (/\t/,$_);

	#multiple mges
	if ($l[1] =~ m/:(\d+)&(\w+)/) {
	$mge = $l[1];
        my @temp = split (/\&/,$mge);

		#hash of all present mges
		for($i=0;$i<=$#temp;$i++) {
		$temp[$i] =~ s/:(\d+)//g;
		$tmge{$l[0]}{$temp[$i]} = 1;
		}


		#if multiple mges do not contain IS_Tn
		$is = "IS_Tn";
		if (!exists $tmge{$l[0]}{$is}) {

			if ($tvar = "Integron" and exists $tmge{$l[0]}{$tvar} and $l[2] =~ m/Integron/) {
                        print "Integron\t$_\n";
                        }

                        elsif ($tvar = "Cellular" and exists $tmge{$l[0]}{$tvar} and $l[2] =~ m/Xer|Cyan|Candidate|Arch1/) {
                        print "Cellular\t$_\n";
                        }

			elsif ($tvar = "CE" and exists $tmge{$l[0]}{$tvar} and $ph{$l[2]} eq 0) {
	                	if ($ce{$l[2]} eq 1 and $l[2] !~ m/Integron/) {
				#if ($ce{$l[2]} eq 1 or $mi{$l[2]} eq 1 and $l[2] !~ m/Integron/) {
				print "CE\t$_\n";
                		}
				elsif ($tvar = "MI" and exists $tmge{$l[0]}{$tvar} and $ce{$l[2]} eq 1 and $ph{$l[2]} eq 1) {
				#elsif ($tvar = "MI" and exists $tmge{$l[0]}{$tvar} and $ime{$l[2]} eq 1) {
				print "MI\t$_\n";
                                }
			}


                	elsif ($tvar = "Phage" and exists $tmge{$l[0]}{$tvar} and $ce{$l[2]} eq 0 and $ph{$l[2]} eq 1) {
                	print "Phage\t$_\n";
                	}

			elsif ($tvar1 = "Phage" and $tvar2 = "CE" and $tvar3 = "MI" and exists $tmge{$l[0]}{$tvar1} and !exists $tmge{$l[0]}{$tvar2} and !exists $tmge{$l[0]}{$tvar3} ) {
                        print "Phage\t$_\n";
                        }


			else {
                        print "nested\t$_\n";
                        }
		}


		#if multiple mges contian IS_Tn
		elsif(exists $tmge{$l[0]}{$is}) {
			if ($tn{$l[2]} eq 1 and $ph{$l[2]} eq 0 and $ce{$l[2]} eq 0 and $l[2] !~ m/Xer|Cyan|Candidate|Arch1/) {
			#if ($tn{$l[2]} eq 1 and $ph{$l[2]} eq 0 and $ce{$l[2]} eq 0 and $mi{$l[2]} eq 0 and $l[2] !~ m/Xer|Cyan|Candidate|Arch1/) {
               		print "IS_Tn\t$_\n";
                	}

 			elsif ($tvar = "Integron" and exists $tmge{$l[0]}{$tvar} and $l[2] =~ m/Integron/) {
                        print "Integron\t$_\n";
                        }

                        elsif ($tvar = "Cellular" and exists $tmge{$l[0]}{$tvar} and $l[2] =~ m/Xer|Cyan|Candidate|Arch1/) {
                        print "Cellular\t$_\n";
                        }
			
			elsif($tn{$l[2]} eq 1 and $ph{$l[2]} eq 1 and $ce{$l[2]} eq 0 and $tvar1 = "Phage_like" and $tvar2 = "Phage" and !exists $tmge{$l[0]}{$tvar1} and !exists $tmge{$l[0]}{$tvar2}) {
			#elsif($tn{$l[2]} eq 1 and $ph{$l[2]} eq 1 and $ce{$l[2]} eq 0 and $mi{$l[2]} eq 0 and $tvar1 = "Phage_like" and $tvar2 = "Phage" and !exists $tmge{$l[0]}{$tvar1} and !exists $tmge{$l[0]}{$tvar2}) {
			print "IS_Tn\t$_\n";
                        }

			elsif($tn{$l[2]} eq 1 and $ph{$l[2]} eq 0 and $tvar1 = "CE" and $tvar2 = "MI" and !exists $tmge{$l[0]}{$tvar1} and !exists $tmge{$l[0]}{$tvar2}) {
                        print "IS_Tn\t$_\n";
                        }

			elsif ($tn{$l[2]} eq 1 and $ph{$l[2]} eq 1 and $tvar1 = "Phage" and $tvar2 = "IS_Tn" and exists $tmge{$l[0]}{$tvar1} and exists $tmge{$l[0]}{$tvar1}) {
                        print "nested\t$_\n";
                        }

			elsif ($tn{$l[2]} eq 1 and $ph{$l[2]} eq 1 and $tvar1 = "Phage_like" and $tvar2 = "IS_Tn" and exists $tmge{$l[0]}{$tvar1} and exists $tmge{$l[0]}{$tvar1}) {
                        print "nested\t$_\n";
                        }

			elsif ($tvar1 = "CE" and $tvar2 = "IS_Tn" and exists $tmge{$l[0]}{$tvar1} and exists $tmge{$l[0]}{$tvar1} and $ph{$l[2]} eq 1 and $ce{$l[2]} eq 0) {
                        print "nested\t$_\n";
                        }



			elsif ($tvar1 = "CE" and exists $tmge{$l[0]}{$tvar1}) {
				if ($ce{$l[2]} eq 1 and $l[2] !~ m/Integron/) {
				#if ($ce{$l[2]} eq 1 or $mi{$l[2]} eq 1 and $l[2] !~ m/Integron/) {
				print "CE\t$_\n";
				}
			}
			
			elsif ($tvar = "MI" and exists $tmge{$l[0]}{$tvar} and $ce{$l[2]} eq 1 and $ph{$l[2]} eq 1) {	
			#elsif ($tvar = "MI" and exists $tmge{$l[0]}{$tvar} and $ime{$l[2]} eq 1) {
                        print "MI\t$_\n";
			}

			elsif ($tvar = "MI" and exists $tmge{$l[0]}{$tvar} and $ce{$l[2]} eq 1 and $ph{$l[2]} eq 1 and $l[2] !~ m/Integron/) {
			#elsif ($tvar = "MI" and exists $tmge{$l[0]}{$tvar} and $mi{$l[2]} eq 1 and $l[2] !~ m/Integron/) {
                        print "MI\t$_\n";
                        }

			elsif ($tvar = "CE" and exists $tmge{$l[0]}{$tvar} and $tn{$l[2]} eq 0) {
				if ($ce{$l[2]} eq 1 and $l[2] !~ m/Integron/) {
				#if ($ce{$l[2]} eq 1 or $mi{$l[2]} eq 1 and $l[2] !~ m/Integron/) {
        	                print "CE\t$_\n";
                	        }
			}
				
			elsif ($tvar = "Phage" and exists $tmge{$l[0]}{$tvar} and $ph{$l[2]} eq 1 and $tn{$l[2]} eq 0) {
                        print "Phage\t$_\n";
                        }

                        elsif ($tvar = "Phage_like" and exists $tmge{$l[0]}{$tvar} and $ph{$l[2]} eq 1 and $tn{$l[2]} eq 0) {
                        print "Phage_like\t$_\n";
                        }

			#elsif ($tvar1 = "Phage_like" and $tvar2 = "Phage" and $tn{$l[2]} eq 1 and $ph{$l[2]} eq 1 and !exists $tmge{$l[0]}{$tvar1} and !exists $tmge{$l[0]}{$tvar2} and $ce{$l[2]} eq 0 and $mi{$l[2]} eq 0) {
                        #print "IS_Tn\t$_\n";
                        #}				
	
			#elsif ($tvar = "IS_Tn" and $tn{$l[2]} eq 1 and exists $tmge{$l[0]}{$tvar}) {

			#	if ($tvar1 = "CE" and $tvar2 = "MI" and $ph{$l[2]} eq 0 and !exists $tmge{$l[0]}{$tvar1} and !exists $tmge{$l[0]}{$tvar1}) {
                        #	print "IS_Tn\t$_\n";
                        #	}

			#	if ($tvar1 = "Phage" and $tvar2 = "Phage_like" and $ce{$l[2]} eq 0 and !exists $tmge{$l[0]}{$tvar1} and !exists $tmge{$l[0]}{$tvar1} and $ph{$l[2]} eq 0) {
                        #print "IS_Tn\t$_\n";
                        #        }
			#}

			else {
			print "nested\t$_\n";
			}

		}

	}	

	else {
	$mge = $l[1];
	$mge =~ s/&//g;
	$mge =~ s/:(\d+)//g;
	print "$mge\t$_\n";
	}	

}
close fh1;
